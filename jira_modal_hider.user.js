// ==UserScript==
// @name            JIRA Modal Dialog Hider
// @namespace       http://atlassian.com/userscripts/support
// @description     Allows JIRA modal dialogs to be temporarily hidden so that you can access the information behind it.
// @include         https://support.atlassian.com/browse/*
// ==/UserScript==

// From http://stackoverflow.com/questions/2246901/how-can-i-use-jquery-in-greasemonkey-scripts-in-google-chrome
function addJQuery(callback) {
    var script = document.createElement("script");
    script.setAttribute("src", "//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js");
    script.addEventListener('load', function() {
        var script = document.createElement("script");
        script.textContent = "window.jQ=jQuery.noConflict(true);(" + callback.toString() + ")();";
        document.body.appendChild(script);
    }, false);
    document.body.appendChild(script);
}

function main() {
    var dialogZindex = null;
    var dimmerZindex = null;

    // Create a toggle button.
    jQ('body').append("<div style=\"position:fixed; top:40px; right:10px; z-index:9999; background-color:#fff;\" id=\"toggle-modal\"><a href=\"#\">Toggle Modal</a></div>");

    // Attach dim toggle function.
    jQ('#toggle-modal a').click(function() {
        if (jQ('.aui-blanket') && jQ('.aui-blanket').is(":visible") && jQ('.aui-blanket').css('z-index') > 0) {
            // There must be a modal dialog up.
            dialogZindex = jQ('.jira-dialog').css('z-index');
            dimmerZindex = jQ('.aui-blanket').css('z-index');
            jQ('.jira-dialog').css('z-index', -1);
            jQ('.aui-blanket').css('z-index', -1);
            jQ('body').css('overflow-y', 'scroll');
        } else if (jQ('.jira-dialog').is(":visible")) {
            // dialog must have been hidden by us, so let's show it.
            jQ('.aui-blanket').css('z-index', dimmerZindex);
            jQ('.jira-dialog').css('z-index', dialogZindex);
            jQ('body').css('overflow', 'hidden');
        }
        return false;
    });
}

// load jQuery and execute the main function
addJQuery(main);
